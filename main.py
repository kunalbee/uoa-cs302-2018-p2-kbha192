import hashlib
import os
import smtplib
import sqlite3
import uuid
import urllib2
import cherrypy
import datetime
import codecs
import json 
import time
import socket
from timerThing import timerThing
from jinja2 import Environment, FileSystemLoader
import base64
import string
from random import *
import threading
import mimetypes

listen_ip = "192.168.1.71"
listen_port = 10001



class MainApp(object):

    def __init__(self):
        self.reportTimer = timerThing(45, self.report) #This is the timer responsible for reporting ever 45 seconds
        self.reportTimer.start()
        self.reportTimer.pause()
        self.hashPassword = None
        self.loggedIn = False
        self.username = None
        self.sendingTo = ""
        self.message = None
        self.searchUser = None
        self.randomCode = None
        self.timerr = None
        self.fileLink = None
        self.file_name = None
        self.content_type = None

    #CherryPy Configuration
    _cp_config = {'tools.encode.on': True, 
                  'tools.encode.encoding': 'utf-8',
                  'tools.sessions.on' : 'True',
                 }                   
    # save sent messages to database table
    def saveSentMessages(self):
        msgTime = datetime.datetime.fromtimestamp(int(time.time())).strftime('%H:%M:%S')
        # open database 
        conn = sqlite3.connect("UserData.db")  
        c = conn.cursor()
        c.execute("INSERT INTO message (sender, destination, stamp, message) VALUES (?,?,?,?)", (self.username, self.sendingTo, msgTime, self.message,))
        conn.commit()
        conn.close()
        return True

    # hashes the password by using username
    def hash_password(self, password):
        salt = "kbha192"
        return hashlib.sha256(password.encode() + salt.encode()).hexdigest() + ':' 

    # check if password entered by user is same as saved password
    def check_password (self, hashed_password, user_password):
        password, salt = hashed_password.split(':')
        return password == hashlib.sha256(user_password.encode() + salt.encode()).hexdigest()

    # check if user exists in databse
    def checkUser(self, username,password):
        conn = sqlite3.connect("UserData.db")
        c = conn.cursor()
        c.execute("SELECT * FROM userInfo WHERE username = ?", (username,))
        names = c.fetchone()
        conn.commit()
        conn.close()
        hashed_password = self.hash_password(password)
        samePass = self.check_password(hashed_password,password)
        if ((names[0] == username) and (samePass) and (username != "")): 
            cherrypy.session['username'] = username
            raise cherrypy.HTTPRedirect('/')
        else:
            raise cherrypy.HTTPRedirect('/login')

    # ability sign up a user
    def signUpUser(self, username,password,email,name):
        conn = sqlite3.connect("UserData.db")
        c = conn.cursor()
        c.execute("SELECT * FROM userInfo WHERE username = ?", (username,))
        user = c.fetchone()
        c.execute("SELECT * FROM userInfo WHERE email = ?", (email,))
        em = c.fetchone()
        conn.close()
        hashed_password = self.hash_password(password)
        if ((user == None) & (em == None) & ((username or password or email or name) != None)):
            c.execute("INSERT INTO userInfo VALUES (?,?,?,?)", (username,hashed_password,email,name))
            conn.commit()
            conn.close()
            server = smtplib.SMTP('smtp.gmail.com',587)
            server.starttls()
            server.login("uoabook@gmail.com", "uoabookpass")
            msg = "Welcome " + name + ", \n \nThank you for signing up on UoABook. We hope you enjoy uyour time browsing throught the website and stay connected to your friends at uni via our messaging services. \n \nIf you have any complaints or issues, please let the me know, I will personally help you to resolve the problem. \n \nRegards, \nKunal B.\nDeveloper of UoABook"
            server.sendmail("uoabook@gmail.com", email, msg)
            server.quit()

    # update user profile after it has been edited
    def updateProfile(self, fullname,position,description,location,picture,encoding, encryption, decryptionKey):
        upTime = datetime.datetime.fromtimestamp(int(time.time())).strftime('%H:%M:%S')
        conn = sqlite3.connect("UserData.db")
        c = conn.cursor()
        c.execute('''UPDATE userProfile SET lastUpdated=?, fullname=?, position=?, description=?, location=?, picture=?, encoding=?, encryption=?, decryptionKey=?, username=?''', (upTime,fullname,position,description,location,picture,encoding, encryption, decryptionKey, cherrypy.session['username']))
        conn.commit()
        conn.close()
        raise cherrypy.HTTPRedirect('/profile')

    # save user info for online users in database
    def saveOnlineUsersInfo(self, ipDic, userDic, locationDic, portDic, lastLoginDic):
        for i in range(0, len(userDic)):
            conn = sqlite3.connect("UserData.db")
            c = conn.cursor()   
            c.execute("DELETE FROM userInfo WHERE username=?", (userDic[i],))
            c.execute("INSERT INTO userInfo (ip, location, port, username) VALUES (?,?,?,?)", (ipDic[i],locationDic[i],portDic[i],userDic[i]))
            conn.commit()
            conn.close()
        return True

    # get user info for online users in the database
    def getOnlineUserInfo(self, username):
        conn = sqlite3.connect("UserData.db")
        c = conn.cursor()
        c.execute("SELECT * FROM userInfo WHERE username = ?", (username,))
        data = c.fetchone()
        conn.close()
        return data

    # find user profile in database
    def locateProfile(self, username):
        conn = sqlite3.connect("UserData.db")
        c = conn.cursor()
        c.execute("SELECT * FROM userProfile WHERE username=?", (username,))
        user = c.fetchone()
        conn.close()
        return user

    # save received messages in database
    def saveReveivedMessage(self, list_dict):
        msgTime = datetime.datetime.fromtimestamp(int(list_dict.get('stamp'))).strftime('%H:%M:%S')
        conn = sqlite3.connect("UserData.db")
        c = conn.cursor()
        c.execute("INSERT INTO message (sender, destination, stamp, message, file, filename, content_type,image,audio,video,other) VALUES (?,?,?,?,?,?,?,?,?,?,?)", (list_dict.get('sender'), list_dict.get('destination'),msgTime,list_dict.get('message'),list_dict.get('file'),list_dict.get('filename'),list_dict.get('content_type'),list_dict.get('image'),list_dict.get('audio'),list_dict.get('video'),list_dict.get('other'),))
        conn.commit()
        conn.close()
        return True

    # If they try somewhere we don't know, catch it here and send them to the right place.
    @cherrypy.expose
    def default(self, *args, **kwargs):
        """The default page, given when we don't recognise where the request is for."""
        Page = "I don't know where you're trying to go, so have a 404 Error."
        cherrypy.response.status = 404
        return Page
 
    # PAGES (which return HTML that can be viewed in browser)
    @cherrypy.expose
    def index(self):   
        try:
            if self.loggedIn:
                self.startTimer()
            # go to userpage if logged in
            online = self.getUsers()
            user = self.locateProfile(cherrypy.session['username'])
            file_loader = FileSystemLoader('templates')
            env = Environment(loader=file_loader)
            template = env.get_template('userPage.html')
            Page = template.render(fullname = user[2],position = user[3], description = user[4], location = user[5],picture = user[6],encoding = user[7],encryption = user[8],decryptionKey = user[9], response = online)
        except KeyError: #There is no username, go back to login
            Page = file("login.html")
        return Page

    # calls report function every 30 seconds
    def startTimer(self):
        threading.Timer(30, self.report(self.username, self.hashPassword))
    
    @cherrypy.expose
    def login(self):
        if self.loggedIn == True:
            raise cherrypy.HTTPRedirect('/profile')
        else:
            return file("login.html")
    
    # pings to user
    @cherrypy.expose
    def ping(self, sender=None):
        if sender == None:
            return "1"
        else:
            return "0"
    
    # allow others to see your profile
    @cherrypy.expose
    @cherrypy.tools.json_in()
    def getProfile(self):
        input_data = cherrypy.request.json
        conn = sqlite3.connect("UserData.db")
        c = conn.cursor()
        c.execute("SELECT * FROM userProfile WHERE username=?", (self.username,))
        user = c.fetchone()
        out_data = {
                'lastUpdated' : user[0],
                'fullname' : user[2],
                'position' : user[3],
                'description' : user[4],
                'location' : user[5],
                'picture' : user[6],
            }
        print out_data
        conn.close()
        out_data = json.dumps(out_data)
        return out_data
    
    # check if random generated code is same as the code user entered
    @cherrypy.expose
    def two_factor(self, userCode=None):
        if (userCode == self.randomCode):
            self.loggedIn = True
            self.getUsers()
            #threading.timer(30, self.report())
            raise cherrypy.HTTPRedirect('/profile')
        else:
            print "NOT THE SAME"
    
    # display two factor html
    @cherrypy.expose
    def showTwoFactor(self):
        return file("templates/two_factor.html")

    # display logout ocnfirm html
    @cherrypy.expose
    def logoutConfirm(self):
        return file("logout.html")

    # get all messages from database and send to html to display
    @cherrypy.expose
    def messenger(self):
        msgString = ''
        if self.sendingTo != None:
            conn = sqlite3.connect('UserData.db')
            c = conn.cursor()
            c.execute(("SELECT * FROM message WHERE sender='{a}' AND destination='{b}' OR sender='{b}' AND destination='{a}'").format(a =self.sendingTo, b=self.username))
            msgString = [dict(zip(['sender', 'destination', 'stamp', 'message'],row)) for row in c.fetchall()]
            conn.close()
            online = self.getUsers()
            user = self.locateProfile(cherrypy.session['username'])
            file_loader = FileSystemLoader('templates')
            env = Environment(loader=file_loader)
            template = env.get_template('messenger.html')
            output = template.render(fullname = user[2], picture = user[6], response = online, name = self.sendingTo, msgs = msgString, fileName = self.file_name, content_type = self.content_type)
            return output
        else:
            online = self.getUsers()
            user = self.locateProfile(cherrypy.session['username'])
            file_loader = FileSystemLoader('templates')
            env = Environment(loader=file_loader)
            template = env.get_template('messenger.html')
            output = template.render(fullname = user[2], picture = user[6], response = online, name = self.sendingTo, msgs = msgString, fileName = self.file_name, content_type = self.content_type)
            return output

    # get other user's profile and send th einfo to html to view it
    @cherrypy.expose
    def viewProfile(self, searchUser=None):
        self.searchUser = searchUser
        if self.loggedIn:
            user_profile = self.get_user_profile()
            print user_profile
            user = self.locateProfile(cherrypy.session['username'])
            file_loader = FileSystemLoader('templates')
            env = Environment(loader=file_loader)
            template = env.get_template('profilePage.html')
            if user_profile.get('lastUpdated') != None:
                lastUp = datetime.datetime.fromtimestamp(int(user_profile.get('lastUpdated'))).strftime('%d-%m-%Y %H:%M:%S')
            else:
                lastUp = "Not Given"
            output = template.render(picture = user[6], fullname = user_profile.get('fullname'), position = user_profile.get('position'), description = user_profile.get('description'), location = user_profile.get('location'), userPic = user_profile.get('picture'), encoding =user_profile.get('encoding'), encryption = user_profile.get('encryption'), decryptionkey = user_profile.get('decryptionkey'), lastUpdated = lastUp  )
            return output
        else:
            raise cherrypy.HTTPRedirect('/login')

    # display register html
    @cherrypy.expose
    def register(self):
        return file("register.html")

    # get user profile and save changes to database
    def saveUserProfile(self, userInfo):
        conn = sqlite3.connect("UserData.db")  
        c = conn.cursor()
        # updateTime = datetime.datetime.fromtimestamp(int(userInfo.get('lastUpdated'))).strftime('%H:%M:%S')
        #c.execute("DELETE FROM userProfile WHERE username=?", (userInfo.get('username'),))
        c.execute("INSERT INTO userProfile (lastUpdated,username, fullname, position, description, location, picture, encoding, encryption, decryptionKey) VALUES (?,?,?,?,?,?,?,?,?,?)", (userInfo.get('lastUpdated'), userInfo.get('username'), userInfo.get('fullname'), userInfo.get('position'), userInfo.get('description'),userInfo.get('location'),userInfo.get('picture'),userInfo.get('encoding'),userInfo.get('encryption'),userInfo.get('decryptionKey')))
        conn.commit()
        conn.close()
        return True
    
    # get other users profile and view it in html
    @cherrypy.expose
    def get_user_profile(self):
        if self.loggedIn:
            conn = sqlite3.connect("UserData.db")
            c = conn.cursor()   
            c.execute("SELECT * FROM userInfo WHERE username=?", (self.searchUser,))
            user = c.fetchone()
            conn.close()
            
            content = { 'profile_username': self.searchUser}
            content['sender'] = self.username
            content = json.dumps(content)
            req = urllib2.Request('http://'+ user[1] + ':' + user[2] + '/getProfile', content, {'Content-Type': 'application/json'})
            response = urllib2.urlopen(req).read()
            response = json.loads(response)
            self.saveUserProfile(response)
            return response
        else:
            return None

    # display edit page html
    @cherrypy.expose
    def edit(self):
        if self.loggedIn:
            user = self.locateProfile(cherrypy.session['username'])
            page = open("editUserPage.html").read().format(fullname = user[1], picture = user[6])
            return page
        else:
            raise cherrypy.HTTPRedirect('/login')

    # reports every 30 seconds so user sytays logged in
    def report(self, username, password):
        if username is None:
            username = self.username
        if password is None:
            password = self.hashPassword
        if (password != self.hashPassword):
            hashPassword = hashlib.sha256((password+username).encode('utf-8')).hexdigest()
        else:
            hashPassword = password
        ip_add = ""
        getIp = json.loads(urllib2.urlopen("https://api.ipdata.co/").read())
        user_ip = getIp['ip']
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        local_ip = s.getsockname()[0]
        s.close()
        if local_ip.startswith("10.10"):
            ip_add = local_ip
            location = "0"
        elif local_ip.startswith("172.2"):
            ip_add = local_ip
            location = "1"
        else:   
            ip_add = user_ip   
            location = "2"
        url = "https://cs302.pythonanywhere.com/report?username="+username+"&password="+hashPassword+"&location="+location+"&ip="+ip_add+"&port="+str(listen_port)+"&enc=0"
        response = urllib2.urlopen(url).read()
        if "0" in response: 
            self.reportTimer.resume()
        return response

    # gets the username to send a message to
    @cherrypy.expose 
    def getUsername(self, nameOfUser):
        self.sendingTo = nameOfUser
        raise cherrypy.HTTPRedirect("/messenger")

    # called form click on send message in html
    @cherrypy.expose
    def messageSender(self, message, fileName):
        self.message = message
        if fileName != "":
            print fileName
            self.send_file(fileName)
        resp = self.sendMessage()
        print resp
        raise cherrypy.HTTPRedirect("/messenger")

    # sends message to user and saves in database
    @cherrypy.expose
    def sendMessage(self):
        info = self.getOnlineUserInfo(self.sendingTo)
        if self.sendingTo:
            ip = info[1]
            port = info[2]
            output = {"sender": self.username, "destination":self.sendingTo,
                    "message": self.message, "stamp":time.time()}
            data = json.dumps(output)
            req = urllib2.Request("http://"+ip+":"+port+"/receiveMessage", data, {'Content-Type': 'application/json'})
            response = urllib2.urlopen(req)
            self.saveSentMessages()
            return response 
 
    # receive messages
    @cherrypy.expose
    @cherrypy.tools.json_in()
    def receiveMessage(self):
        input_data = cherrypy.request.json
        self.saveReveivedMessage(input_data)
        return '0: Successfully received'  
    
    # send a file to a user
    @cherrypy.expose
    def send_file(self, filename):
        info = self.getOnlineUserInfo(self.sendingTo)
        if self.loggedIn:
            ip = info[1]
            port = info[2]
            path = "files/"
            fullPath = os.path.join(path, filename)
            fileOpen = open(fullPath, 'rb')
            fileRead = fileOpen.read()
            fileEncoded = base64.encodestring(fileRead)
            content_type = mimetypes.guess_type(filename,strict = False)[0]
            content = {"sender": self.username, "destination": self.sendingTo, "file": fileEncoded, "filename": filename, "content_type": content_type, 'stamp': time.time(), 'hashing': 0, 'encryption': 0 }
            content = json.dumps(content)
            req = urllib2.Request('http://'+ ip + ':' + port + '/receiveFile', content, {'Content-Type': 'application/json'})
            response = urllib2.urlopen(req).read()
            print response
            return response

    # recieve a file form another user and save in in directory
    @cherrypy.expose
    @cherrypy.tools.json_in()   
    def receiveFile(self):
        input_data = cherrypy.request.json
        file = base64.b64decode(input_data['file'])
        path = "files/"
        full_path = os.path.join(path, input_data['filename'])
        with open(full_path, 'wb') as f:  
            f.write(file)
        return '0: File Successfully received'  
    
    # gets user info and displays userPage html
    @cherrypy.expose
    def profile(self):
        try:
            online = self.getUsers()
            if self.loggedIn:
                user = self.locateProfile(cherrypy.session['username'])
                file_loader = FileSystemLoader('templates')
                env = Environment(loader=file_loader)
                template = env.get_template('userPage.html')
                output = template.render(fullname = user[2], position = user[3], description = user[4], location = user[5], picture = user[6],encoding = user[7], encryption = user[8], decryptionKey = user[9], response = online)
                return output
            else:
                self.loggedIn = False
                self.username = None
                raise cherrypy.HTTPRedirect('/login')
        except:
            raise cherrypy.HTTPRedirect('/login')

    # gets all users on the server and calls saveOnlineUsersInfo function to save them in database
    @cherrypy.expose
    def getUsers(self):
        # print "USERNAME AND PASSWORD", self.username, self.hashPassword
        if ((self.username != None) and (self.hashPassword != None)):
            url = "https://cs302.pythonanywhere.com/getList/?username=" + self.username + "&password=" + self.hashPassword + "&enc=0" + "&json=1"
            response = urllib2.urlopen(url).read()       
            response = json.loads(response)
            ipDic = []
            userDic = []    
            portDic = []
            locationDic = []
            lastLoginDic = []
            for i in range (0,len(response)):
                ipDic.append(response[str(i)]['ip'])
                userDic.append(response[str(i)]['username'])
                portDic.append(response[str(i)]['port'])
                locationDic.append(response[str(i)]['location'])
                lastLoginDic.append(response[str(i)]['lastLogin'])
            self.saveOnlineUsersInfo(ipDic, userDic, locationDic, portDic, lastLoginDic)
            return response 
        else:
            raise cherrypy.HTTPRedirect('/login')

    @cherrypy.expose    
    def sum(self, a=0, b=0): #All inputs are strings by default
        output = int(a)+int(b)
        return str(output)

    # called when save buton clicked and calls updateProfile to save changes in database     
    @cherrypy.expose   
    def editUser(self,fullname=None,position=None,description=None,location=None,picture=None,encoding=None, encryption=None, decryptionKey=None):
        if self.loggedIn:
            self.updateProfile(fullname,position,description,location,picture,encoding, encryption, decryptionKey)
        else:
            self.loggedIn = False
            self.username = None
            raise cherrypy.HTTPRedirect('/login')

    # LOGGING IN
    # first stage of loggin in, if username and password correct, sends random code to email for 2FA
    @cherrypy.expose
    def signin(self, username=None, password=None):
        """Check their name and password and send them either to the main page, or back to the main login screen."""
        error = self.authoriseUserLogin(username,password)
        if (error == 0):
            char = string.ascii_letters+string.punctuation+string.digits
            self.randomCode = "".join(choice(char) for i in range(randint(6,12)))
            email = username + "@aucklanduni.ac.nz"
            server = smtplib.SMTP('smtp.gmail.com',587)
            server.starttls()
            server.login("uoabook@gmail.com", "uoabookpass")
            msg = "Welcome " + username + ", \n \nWelcome back to UoABook. Your code for two factor authorisation is:\n\n" + self.randomCode + "\n\nPlease enter the code to login into UoABook. \nRegards,\nKunal\nCEO"
            server.sendmail("uoabook@gmail.com", email, msg)
            server.quit()
            self.username = username
            raise cherrypy.HTTPRedirect('/showTwoFactor')
        else:
            self.loggedIn = False
            self.username = None
            raise cherrypy.HTTPRedirect('/login')
    
    # checks if user login is valid
    def authoriseUserLogin(self, username, password):
        response = self.report(username,password)
        if response == "0, User and IP logged":
            hashPassword = hashlib.sha256((password+username).encode('utf-8')).hexdigest()
            self.hashPassword = hashPassword
            self.username = username
            cherrypy.session['username'] = username
            return 0
        else:
            return 1
    # checks if user successfuly logged out
    def authoriseUserLogout(self, username, password):
        url = "https://cs302.pythonanywhere.com/logoff/?username=" + self.username + "&password=" + self.hashPassword
        response = urllib2.urlopen(url).read()
        if response == "0, Logged off successfully":
            return 0
        else:
            return 1
  
    # registering a new user
    @cherrypy.expose
    def signup(self, username=None, password=None, email=None, name=None):
        """Check their name and password and send them either to the main page, or back to the main login screen."""
        self.signUpUser(username,password,email,name)

    # logging out user
    @cherrypy.expose
    def signout(self, username=None, password=None ):
        """Logs the current user out, expires their session"""
        if self.loggedIn:
            error = self.authoriseUserLogout(username,password)
            if (error == 1):
                print ("Didnt Log Out")
            else:
                cherrypy.lib.sessions.expire()
                self.loggedIn = False
                raise cherrypy.HTTPRedirect('/login')
        else:
            self.loggedIn = False
            self.username = None
            raise cherrypy.HTTPRedirect('/login')

# the main thing          
def runMainApp():
    #clear cmd
    os.system('cls')
    # Create an instance of MainApp and tell Cherrypy to send all requests under / to it. (ie all of them)
    cherrypy.tree.mount(MainApp(), "/", {
        '/':
            {
                    'tools.sessions.on' : True,
                    'tools.staticdir.root' : os.path.abspath(os.getcwd()),
                    'tools.staticdir.dir' : os.path.abspath(os.path.dirname(__file__))
            },
            '/files':
                {
                    'tools.staticdir.on' : True,
                    'tools.staticdir.root' : './files',
                }
        })


    # Tell Cherrypy to listen for connections on the configured address and port.
    cherrypy.config.update({'server.socket_host': listen_ip,
                            'server.socket_port': listen_port,
                            'engine.autoreload.on': True,
                           })
    print "=============================================================================="
    print "UoABook - Running"
    print "By the Dumbest Programmer of all time"
    print "iamkunalbee"
    print "=============================================================================="                       
    
    # Start the web server
    cherrypy.engine.start()

    # And stop doing anything else. Let the web server take over.
    cherrypy.engine.block()

#Run the function to start everything
runMainApp()
